Provisioning scripts to set up wirebot.
The setup consists of three docker containers running on a docker host.
The provisioning takes care of setting up the docker host.
Each docker container contains an essential element of wirebot: the wire-webapp, the browser logging into the wire-webapp and lambdabot.

# Building:
vagrant up --provider=aws
vagrant ssh

docker build -t webapp /vagrant/wire-webapp/
docker build -t wirebot /vagrant/lambdabot/

docker run --rm --name wirebot -p 8012:8012 -ti wirebot
docker run --rm --name webapp --link wirebot:wirebot -p 8888:8888 webapp



# Synchronize with upstream repo and apply our fixes to the latest codebase
git remote add upstream https://github.com/wireapp/wire-webapp
git fetch upstream
git rebase dev


